Pod::Spec.new do |spec|
    spec.name                     = 'MyCocoaPod'
    spec.version                  = '0.0.3'
    spec.homepage                 = 'Link to a Kotlin/Native module homepage'
    spec.source                   = { 
                                      :http => 'https://gitlab.com/api/v4/projects/55839086/packages/maven/org/jetbrains/kotlinx/multiplatform-library-template/library-kmmbridge/0.0.3/library-kmmbridge-0.0.3.zip',
                                      :type => 'zip',
                                      :headers => ['Accept: application/octet-stream']
                                    }
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'Some description for a Kotlin/Native module'
    spec.vendored_frameworks      = 'ShareFramework.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '13'
            
            
end