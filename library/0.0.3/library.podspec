Pod::Spec.new do |spec|
    spec.name                     = 'library'
    spec.version                  = '0.0.3'
    spec.homepage                 = 'https://dnse.com'
    spec.source                   = { 
                                      :http => 'https://gitlab.com/api/v4/projects/55839086/packages/maven/org/jetbrains/kotlinx/multiplatform-library-template/library-kmmbridge/0.0.3/library-kmmbridge-0.0.3.zip',
                                      :type => 'zip',
                                      :headers => ['Accept: application/octet-stream']
                                    }
    spec.authors                  = ''
    spec.license                  = ''
    spec.summary                  = 'KMMBridgeSampleKotlin'
    spec.vendored_frameworks      = 'library.xcframework'
    spec.libraries                = 'c++'
    spec.ios.deployment_target = '13'
            
            
end